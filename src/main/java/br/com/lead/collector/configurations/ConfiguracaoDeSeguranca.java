package br.com.lead.collector.configurations;

import br.com.lead.collector.security.FiltroDeAutenticacao;
import br.com.lead.collector.security.FiltroDeAutorizacao;
import br.com.lead.collector.security.JWTUtil;
import br.com.lead.collector.services.LoginUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class ConfiguracaoDeSeguranca extends WebSecurityConfigurerAdapter {

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private LoginUsuarioService loginUsuarioService;

    // Solução útil para vários GETS (Solução elegante)
    private static final String[] PUBLIC_MATCHERS_GET = {
            "/leads",
            "/leads/*",
            "/produtos"
    };

    private static final String[] PUBLIC_MATCHERS_POST = {
            "/leads",
            "/usuario",
            "/login"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Desabilita o preenchimento de token de segurança de formulário de cadastro(útil para desenvolvimento WEB MVC)
        // Em API RestFull não é preciso se preocupar, pois não há formulário enviado pelo sistema para o usuário
        // a comunicação é feita por JSON
        http.csrf().disable();
        http.cors();

        http.authorizeRequests()
                /* Solução útil para um GET
                .antMatchers(HttpMethod.GET, "/leads").permitAll()
                 */
                // Solução útil para vários GETS
                .antMatchers(HttpMethod.GET, PUBLIC_MATCHERS_GET).permitAll()
                .antMatchers(HttpMethod.POST, PUBLIC_MATCHERS_POST).permitAll()
                .anyRequest().authenticated();

        // Como API RESTFULL é STATELESS, podemos criar uma política para não guardar sessão
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilter(new FiltroDeAutenticacao(authenticationManager(), jwtUtil));
        http.addFilter(new FiltroDeAutorizacao(authenticationManager(), jwtUtil, loginUsuarioService));
    }

    // Libera acesso na API a partir de qualquer origem de outros servidores
    @Bean
    protected CorsConfigurationSource configuracaoDeCors(){
        final UrlBasedCorsConfigurationSource cors = new UrlBasedCorsConfigurationSource();
        cors.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return cors;
    }

    // Cria o objeto em memória e fornece para quem está pedindo usando o @Autowired
    @Bean
    BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(loginUsuarioService).passwordEncoder(bCryptPasswordEncoder());
    }
}
