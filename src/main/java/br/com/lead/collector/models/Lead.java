package br.com.lead.collector.models;

import br.com.lead.collector.enums.TipoLeadEnum;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NegativeOrZero;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String nome;
    private String email;
    private LocalDate data;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Produto> produtos;

    @DecimalMin( value = "0.1", message = "Vai que vai")
    private TipoLeadEnum tipoLead;

    public Lead(){
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public TipoLeadEnum getTipoLead() {
        return tipoLead;
    }

    public void setTipoLead(TipoLeadEnum tipoLead) {
        this.tipoLead = tipoLead;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
}
