package br.com.lead.collector.controllers;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.dtos.LeadDTO;
import br.com.lead.collector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/leads")
public class LeadController {

    @Autowired
    private LeadService leadService;


    @PostMapping
    public ResponseEntity<Lead> registarLead(@RequestBody @Valid Lead lead){
        Lead leadObjeto = leadService.salvarLead(lead);
        return ResponseEntity.status(201).body(lead);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarLead(@PathVariable int id){
        try{
            leadService.deletarLead(id);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping
    public Iterable<Lead> exibirTodos(@RequestParam(name = "tipoDeLead", required = false) TipoLeadEnum tipoLeadEnum){
        if(tipoLeadEnum != null){
            Iterable<Lead> leads = leadService.buscarTodosPorTipoLead(tipoLeadEnum);
            return leads;
        }
        Iterable<Lead> leads = leadService.buscarTodos();
        return leads;
    }

    @GetMapping("/{id}")
    public Lead bucarPorId(@PathVariable(name = "id") int id){
        try{
            Lead lead = leadService.buscarPorId(id);
            return lead;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Lead atualizarLead(@PathVariable(name = "id") int id, @RequestBody Lead lead){
        try{
            Lead leadobjeto = leadService.atualizarLead(id, lead);
            return leadobjeto;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
